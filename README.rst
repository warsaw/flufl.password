==============
flufl.password
==============

Password hashing and verification.

The `flufl.password` library provides hashing and verification of passwords.


Author
======

`flufl.password` is Copyright (C) 2004-2015 Barry Warsaw <barry@python.org>

Licensed under the terms of the GNU Lesser General Public License, version 3
or later.  See the COPYING.LESSER file for details.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.password
 * Report bugs at: https://gitlab.com/warsaw/flufl.password/issues
 * Code hosting: git@gitlab.com:warsaw/flufl.password.git
 * Documentation: http://fluflpassword.readthedocs.org/
