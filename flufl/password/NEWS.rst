=======================
NEWS for flufl.password
=======================

1.4 (201X-XX-XX)
================


1.3 (2014-09-24)
================
 * Fix documentation bug.  (LP: #1026403)
 * Purge all references to `distribute`.
 * Describe the switch to git and the repository move.


1.2.1 (2012-04-19)
==================
 * Add classifiers to setup.py and make the long description more compatible
   with the Cheeseshop.
 * Other changes to make the Cheeseshop page look nicer.  (LP: #680136)
 * setup_helper.py version 2.1.


1.2 (2012-01-23)
================
 * Fix some packaging issues.
 * Remove tox.ini.
 * Bump Copyright years.
 * Update standard template.
 * Eliminate the need to use 2to3, and fix some Python 3 deprecations.


1.1.1 (2012-01-01)
==================
 * Ensure all built-in schemes are registered by importing them in the
   __init__.py file.


1.1 (2011-12-31)
================
 * Add user-friendly password generation API.


1.0 (2011-12-31)
================
 * Initial release.
