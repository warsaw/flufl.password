==================================================
flufl.password - Password hashing and verification
==================================================

This package is called `flufl.password`.  It provides for hashing and
verification of passwords.


Requirements
============

`flufl.password` requires Python 2.6 or newer, and is compatible with Python
3.


Documentation
=============

A `simple guide`_ to using the library is available within this package, in
the form of doctests.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.password
 * Report bugs at: https://gitlab.com/warsaw/flufl.password/issues
 * Code hosting: git@gitlab.com:warsaw/flufl.password.git
 * Documentation: http://fluflpassword.readthedocs.org/

You can install it with `pip`::

    % pip install flufl.password

You can grab the latest development copy of the code using git.  The master
repository is hosted on GitLab.  If you have git installed, you can grab
your own branch of the code like this::

    $ git clone git@gitlab.com:warsaw/flufl.password.git

You may contact the author via barry@python.org.


Copyright
=========

Copyright (C) 2011-2015 Barry A. Warsaw

This file is part of flufl.password.

flufl.password is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

flufl.password is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with flufl.password.  If not, see <http://www.gnu.org/licenses/>.


Table of Contents
=================

.. toctree::

    docs/using.rst
    NEWS.rst

.. _`simple guide`: docs/using.html
